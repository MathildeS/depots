**Se connecter**


>  Dès l’ouverture :

* L’executable demande : “ Donner une commande ( l’authentification est     obligatoire avant toute autre commande ) “
    * Taper     “Authentification”


>  le vendeur clique sur identifiant
 

*  Executable demande identifiant
    *  Le vendeur entre son identifiant


>  le vendeur clique sur mot de passe


*  L'exécutable demande mot de passe

    *  Le vendeur entre son mot de passe

        *   Si ça ne marche pas (failure ) → l'exécutable  renvoie un message “Le mot de passe ou le login est incorrect”

        *   Sinon, renvoie  succès →message “vous êtes connectés”

**Recherches :**

*  L’exécutable demande “ Entrez une commande”
    *   Vendeur entre “Recherche”

*  L’exécutable envoie “ Ville agence de départ” → avec une liste de ville où piocher
    *   Vendeur entre la ville

*  L’exécutable envoie “ Date début location :” → avec une liste de ville où piocher
    *   Vendeur entre la date de début de location


*  L’exécutable envoie “ Date fin de location :” → avec une liste de ville où piocher
    *  Vendeur entre la date de fin de location

*  L’exécutable envoie “ Km par jour que vous pensez effectuer avec un max de 500 Km :”
    *  Vendeur entre le nombre de km que le client pense effectuer au max

*  L’exécutable envoie “ Quel montant entre 15 et 300€ par jour” → avec une liste de ville où piocher
    *  Vendeur entre le montant max 

*  L’exécutable propose des voitures

*  L’exécutable envoie “ Type de véhicule” → avec une liste de type de véhicules
    *  Vendeur entre le type de véhicule

*  L’exécutable envoie “ Marque” → avec une liste de marque
    *  Vendeur entre la marque du véhicule

*  L’exécutable envoie “ Nombre de portes”
    *  Vendeur entre le nombre de portes

*  L’exécutable envoie “ Transmission” 
    *  Vendeur entre la transmission

*  L’exécutable envoie “ Moteur” → avec une liste moteurs
    *  Vendeur entre le moteur

*  L’exécutable envoie “ Puissance moteur” → avec une liste de puissance
    *  Vendeur entre la puissance moteur

*  L’exécutable propose des voitures



**Création du compte :** 

>  le vendeur clique sur créer un compte


    *   numéros_vendeur
    *   nom 
    *   prénom
    *   identifiant de l’agence
    *   adresse mail
    *   nom d’utilisateu
    *   mot de passe

*  Exécutable demande confirmation des données 

    *   Si oui, envoie mail confirmation de création : message “ Un mail vous a été envoyé “
    *   Si non, il retourne au début où demande la connexion

*  “ Entrer une nouvelle commande”

**Accès fiche client :**


>  le vendeur, une fois connecté doit cliquer sur créer fiche client


*  Le vendeur entre “Création fiche client” dans la commande

*  Executable demande nom
    *  Vendeur entre nom du client

*  Exécutable demande prénom
    *  Vendeur entre prénom du client

*  Executable demande date de naissance
    *  Vendeur entre date de naissance du client

*  Exécutable demande permis → vérifier le permis
    *  Vendeur entre permis  du client

*  Exécutable demande numéros téléphone
    *  Vendeur entre numéros téléphone du client

*  Executable demande adresse
    *  Vendeur entre  adresse du client

*  Executable demande ville 
    *  Vendeur entre  ville du client

*  Executable demande  code postal
    *  Vendeur entre code postal du client

*  Executable demande pays
    *  Vendeur entre pays du client

*  Executable demande adresse mail
    *  Vendeur entre adresse mail du client

*  Exécutable demande confirmation des données 
    *  Le vendeur valide

*  Exécutable envoie message → “ La fiche a été créée”

*  “ Entrer une nouvelle commande”


>  ou / le vendeur veut avoir accès à une fiche client
    il doit cliquer sur consulter fiche client
    

*  Le client doit entrer la commande “Consulter fiche client NOM PRÉNOM N° FICHE (id client)”

*  Exécutable demande “ Que voulez vous consulter ?“
    *  Si le vendeur entre “Consulter -help” → révèle les différentes possibilités de réponse :  “Réservation”, “Données personnelles”, “Contrat”

*  “Réservation” → exécutable renvoie “ Consulter ou créer ?”
    *  Le vendeur répond “Créer”  :

*  Exécutable demande “Adresse de l’agence”
    *  Vendeur entre l’adresse

*  Exécutable demande “Date début location”
    *  Vendeur entre date début location

*  Exécutable demande “Km max à effectuer par jour”
    *  Vendeur entre les km

*  Exécutable demande “Immatriculation du véhicule loué”
< On part sur le principe que la recherche effectuée initialement nous a dit que le véhicule était dispo >
    *   Vendeur entre immatriculation du véhicule

*   Exécutable demande “ Retour dans la même agence ?”
    *   Si oui → “ Pas de taxe supplémentaire”
    *   Si non → “Cela engage un supplément de 100€ hors ttc”

*   Exécutable renvoie le devis : dates, immatriculation, si retour dans une autre agence +100€,  les taxes par rapport au véhicule, acompte et donc le prix “final”

*   Exécutable demande “Valider la réservation ?”
    *   Vendeur répond “oui” → renvoie “ Passer au paiement”
   
*   Exécutable demande “N° carte bleue”
    *   Si n° non valide au bout de 3 tentatives ( ie < 16 < )→ annulation de la réservation
    *   Sinon ( ie = 16 chiffres ) l’exécutable demande “Crypto :”

*   Vendeur entre crypto
    *   Si crypto  non valide au bout de 3 tentatives → annulation de la réservation

*   Après vérification (crypto = 3 chiffres) l’exécutable demande “ Date d’expiration au format MM/AA”
    *   Si en dessous d’une certaine date → non valide
    *   Sinon :  demande confirmation

*   Valide → envoie message “ Votre réservation a été effectuée”

*   Renvoie id de la réservation

*   Exécutable envoie “ À présent, nous allons passer au contrat”
    *   Le vendeur répond “Consulter”  :

*   Demande “ N° réservation”
    *   Vendeur entre l’id

*   Exécutable renvoie toutes les infos liées à cette réservation
      Demande “ Voulez vous annuler ?”
    *   Si “oui” → demande “Confirmation”
    *   Répond “Confirmer”

*   Renvoie “Réservation (id reservation) supprimée”

*   “ Entrer une nouvelle commande”


**Accès contrat**


*  Exécutable demande “ Que voulez vous consulter ?“
    *   “Contrat”

*  Exécutable demande “ Début ou fin du contrat ?”
    *   Si “ Début “ → Le résumé du contrat 

        *  Exécutable demande “ Signature numérique du client ”
            *   Le client entre Première lettre prénom et nom

    *   Si “ Fin” → Le résumé du contrat

*   Demande “ Voiture en bon état ?”
    *   Si ‘oui’ → “ L’acompte sera remboursé”
    *   Si ‘non’ → “ L’acompte sera prélevé avec le paiement”

*  Demande “ Kilométrage par jour réel effectué < prévu“
    *  “Oui” → Renvoie “ Vous n’aurez pas de surcoût”
    *  “Non” → Renvoie “ Un supplément de 1€ par km en plus”

*   Modifie prix final 
  
*   Renvoie “ Le nouveau prix est “ + prix

*   Demande confirmation
    *Vendeur confirme

*   “ Vous avez été prélevé d’un montant de “ + prix

*   “ Contrat classé dans la fiche client”

*   “ Entrer une nouvelle commande”


>  le vendeur veut avoir accès aux voitures dans les agences
 
 
*   Le client doit entrer la commande “Consulter les véhicules dans le parc”

*   Exécutable demande “ Adresse agence “ (peut avoir accès à l’help)
    *   Vendeur entre nom agence

*   Exécutable renvoie la liste des voitures 
    *   Le vendeur entre une immatriculation ou l’id 

*   Renvoie si elle est louée, en entretien ou disponible
    *   Le vendeur peut aussi “Entretien”

*   Exécutable demande “ Quel véhicule ?”
    *   Vendeur donne immatriculation ou id 

*   Exécutable demande “Confirmer ?”
    *   Vendeur répond  “Oui” → “ Véhicule envoyé en entretien”
    *   Si “Non” → “ Entrez une nouvelle commande”


**Siège**


>  Se connecte comme le vendeur en tant que siège

>  Accès

*   Exécutable demande : “ Entrer commande”
    *   Siège peut entrer :
        *   N° réservation (ie id) :
        *   Donne accès à toutes les données
        *   Immatriculation d’un véhicule : 
        *   Donne accès à toutes les données
        *   Nom d’un client + sa fiche ( id_client): donne accès à toutes les données
        *   Nom agence ( ie id_agence):
            *   Siège demande “ Parc” → Liste des véhicules dans le parc
            *   Siège demande “ Réservation” → Liste des réservation dans telle agence 
            *   Siège demande “ Client” → Liste des clients dans telle agence
→ Permettra de faire des statistiques


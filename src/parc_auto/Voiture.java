package parc_auto;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import action.Application;
import bdd.VoitureManager;
import squelette.SingletonConnection;


public class Voiture extends AbstractAction{

	/**
	 * Attributs de la voiture
	 */
	private int id_voiture;
	
	private String marque; 

	private String modele;

	private String transmission;

	private String immatriculation;

	private float nb_km_compteur;

	private Entretien entretien;

	private int prix;
	
	private Application appli;

	/**
	 * Getters
	 */
	public int getPrix() {
		return this.prix;
	}

	public int getId_voiture() {
		return id_voiture;
	}
	public String getImmatriculation() {
		return immatriculation;
	}

	public String getMarque() {
		return marque;
	}

	public String getModele() {
		return modele;
	}

	public String getTransmission() {
		return transmission;
	}

	public float getNb_km_compteur() {
		return nb_km_compteur;
	}
	
	public Entretien getEntretien() {
		return this.entretien;
	}

	public void setEntretien(Entretien entretien) {
		this.entretien = entretien;
	}
	
	/**
	 * Constructeur 1
	 * @param id_voiture
	 * @param marque
	 * @param modele
	 * @param transmission
	 * @param immatriculation
	 * @param nb_compteur
	 * @param entretien2
	 * @param nb_km_compteur2
	 */
	public Voiture( String marque, String modele, String transmission, String immatriculation, float nb_km_compteur, Entretien entretien) {

		this.marque = marque;
		this.modele = modele;
		this.transmission = transmission;
		this.immatriculation = immatriculation;
		this.nb_km_compteur = nb_km_compteur;
		this.entretien = entretien;
	}

	/**
	 * Constructeur 2
	 * @param id_voiture
	 */
	public Voiture(int id_voiture) {
		this.id_voiture = id_voiture;
	}
	
	/**
	 * Constructeur 3
	 */
	public Voiture() {
	}
	
	/**
	 * Constructeur 4
	 * @param texte
	 * @param appli
	 */
	public Voiture(String texte, Application appli) {
		super(texte);
		this.appli = appli;
	}
	/**
	 * Sauvegarde dans la base de donnees des voitures
	 * @param id
	 * @throws SQLException
	 */
	private void saveBDD() throws SQLException {

		// INSERT INTO entretien
		Connection conn = SingletonConnection.getInstance();

		//			entretien.saveBDD();

		String sqlE = "INSERT INTO Entretien() VALUES ()";
		PreparedStatement stateE = conn.prepareStatement(sqlE);
		
		// INSERT INTO voiture

		String sqlV = "INSERT INTO  voiture( marque, modele, transmission, immatriculation, nb_km_compteur)  VALUES (?,?,?,?,?)";
		PreparedStatement stateV = conn.prepareStatement(sqlV);
		stateV.setString(1, this.marque);
		stateV.setString(2, this.modele);
		stateV.setString(3, this.transmission);
		stateV.setString(4, this.immatriculation);
		stateV.setFloat(5, this.nb_km_compteur);

		//execution
		stateV.execute();
		stateV.close();
	}

	public void actionPerformed(ActionEvent e) {
		String immatriculation = JOptionPane.showInputDialog(null, "Immatriculation du vehicule","Consulter voiture",JOptionPane.QUESTION_MESSAGE);
		VoitureManager voituremanag = new VoitureManager();
		int id_voiture= 0;
		try {
			id_voiture = voituremanag.bringBackId(immatriculation);
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		List<String> list_donnee = null;
		try {
			list_donnee =  voituremanag.recupDonneesVoiture(id_voiture);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String entre = null;
		if(list_donnee.get(6).equals("oui")) {
			entre = "oui";
		}
		else {
			entre = "non";
		}
		String message = "Resume sur ce vehicule :\n identifiant du vehicule : "+id_voiture+"\n"+
				"Marque : "+list_donnee.get(1)+" Modele : "+list_donnee.get(2)+" Immatriculation : "+list_donnee.get(0)+
				"\nTransmission : "+ list_donnee.get(3)+"      Prix (/jour) : "+list_donnee.get(4)+
				"\nEntretien : "+entre+"     Location : "+list_donnee.get(8);
		JOptionPane.showMessageDialog(null, message,"Consultation de vehicule",JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Methode recuperant le type du vehicule
	 * @param id_voiture
	 * @return le type 
	 * @throws SQLException
	 */
	public String typeVoiture(int id_voiture) throws SQLException {
		// Connexion 
		Connection conn = SingletonConnection.getInstance();

		// Requ�te sql pour r�cup�rer le type 
		String sql = "SELECT type \n FROM type AS t, voiture AS v \n WHERE t.id_type = v.id_type \n" + 
				"AND v.id_voiture = "+"'"+id_voiture+"'";
		PreparedStatement statement = conn.prepareStatement(sql);
		ResultSet type = statement.executeQuery();
		
		// On recupere id_type
		if(type.next()) {
			return type.getString("type");
		}
		else {
			return "Ce vehicule ne possede pas de caracteristique type --> probleme";
		}
	}
	
	/**
	 * Methode recuperant la puissance du vehicule
	 * @param id_voiture
	 * @return la puissance 
	 * @throws SQLException
	 */
	public String puissanceVoiture(int id_voiture) throws SQLException {
		// Connexion 
		Connection conn = SingletonConnection.getInstance();

		// Requete sql pour recuperer le puissance 
		String sql = "SELECT puissance \n FROM puissance AS p, voiture AS v \n WHERE p.id_puissance = v.id_puissance \n" + 
				"AND v.id_voiture = "+"'"+id_voiture+"'";
		Statement statement = conn.createStatement();
		ResultSet puissance = statement.executeQuery(sql);
		
		if(puissance.next()) {
			return puissance.getString("puissance");
		}
		else {
			return "Ce vehicule ne possede pas de caracteristique puissance --> probleme";
		}
	}

	/**
	 * Methode recuperant le moteur du vehicule
	 * @param id_voiture
	 * @return le moteur 
	 * @throws SQLException
	 */
	public String moteurVoiture(int id_voiture) throws SQLException {
		// Connexion 
		Connection conn = SingletonConnection.getInstance();

		// Requete sql pour recuperer le moteur 
		String sql = "SELECT moteur \n FROM moteur AS m, voiture AS v \n WHERE m.id_moteur = v.id_moteur \n" + 
				"AND v.id_voiture = "+"'"+id_voiture+"'";
		PreparedStatement statement = conn.prepareStatement(sql);
		ResultSet moteur = statement.executeQuery();
		
		if(moteur.next()) {
			return moteur.getString("moteur");
		}
		else {
			return "Ce vehicule ne possede pas de caracteristique moteur --> probleme";
		}
	}
	
	/**
	 * Test creation des voitures
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		// Creer une voiture

		Scanner scan = new Scanner(System.in);
		System.out.println("Marque :");
		String marque = scan.nextLine();
		System.out.println("Modele :");
		String modele = scan.nextLine();
		System.out.println("Transmission :");
		String transmission = scan.nextLine();
		System.out.println("Immatriculation :");
		String immatriculation = scan.nextLine();
		System.out.println("Nombre de kilometres au compteur :");
		float nb_km_compteur = scan.nextFloat();

		Voiture voiture = new Voiture(marque, modele, transmission, immatriculation, nb_km_compteur, null);

		Entretien.demandeInfoUtilisateur(voiture);

		VoitureManager.save(voiture);
		int id = VoitureManager.bringBackId(immatriculation);
		System.err.println(id);
		scan.close();

		Voiture polo = new Voiture();
		System.out.println(polo.typeVoiture(2));
	}
}
package parc_auto;

import java.sql.ResultSet;
import java.sql.SQLException;

import squelette.SingletonConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class Type {

	/**
	 * Attributs type
	 */
	private int id_type;

	private String type;
	
	/**
	 * Permettant connexion a BDD
	 */
	private Connection conn;

	/**
	 * Constructeur 1
	 * @param id_type
	 * @param type
	 */
	public Type(int id_type, String type) {
		this.id_type = id_type;
		this.type = type;
	}
	
	/**
	 * Constructeur 2
	 * @param id_type
	 */
	public Type(int  id_type) {
		this.id_type = id_type;
	}
	
	/**
	 * M�thode calcul taux
	 * @param prixdebase
	 * @return
	 * @throws SQLException
	 */
	public float prixPlusTaux(int prixdebase, int id_voiture) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql2 = "SELECT id_type FROM voiture WHERE id_voiture = ?";
		PreparedStatement statement =  conn.prepareStatement(sql2);
		statement.setInt(1,id_voiture);
		ResultSet type = statement.executeQuery();
		if(type.next()) {
			id_type = type.getInt("id_type");
		}
		float tauxT = 0;
		float prix = 0;
		if (id_type == 1) {
			tauxT = (float) 0.001;
			prix = (float) prixdebase*tauxT;

		}
		else if (id_type == 2) {
			tauxT = (float) 0.0075;
			prix =  (float)prixdebase*tauxT;

		}
		else if (id_type == 3) {
			tauxT = (float) 0.0085;
			prix = (float)prixdebase*tauxT;
		}
		else if (id_type == 4) {
			tauxT = (float) 0.02;
			prix = (float)prixdebase*tauxT;

		}
		else if (id_type == 5) {
			tauxT = (float) 0.05;
			prix = (float)prixdebase*tauxT;
		}
		else if (id_type == 6) {
			tauxT = (float) 0.02;
			prix = (float)prixdebase*tauxT;
		}
		else if (id_type == 7) {
			tauxT = (float) 0.025;
			prix = (float)prixdebase*tauxT;
		}
		else if (id_type == 8) {
			tauxT = (float) 0.025;
			prix = (float)prixdebase*tauxT;
		}
		else if (id_type == 9) {
			tauxT = (float) 0.02;
			prix = (float)prixdebase*tauxT;
		}
		else if (id_type == 10) {
			tauxT = (float) 0.03;
			prix = (float)prixdebase*tauxT;
		}

		return prix;
	}
}

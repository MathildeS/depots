package squelette;


import java.awt.event.ActionEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import action.Application;

/**
 * Classe mere utilisateur 
 * @author groupe LocHappy
 *
 */
public class Utilisateur extends AbstractAction {

	/**
	 * Attributs de l'utilisateur
	 */
	private String identifiant;

	private String mot_de_passe;

	private Application application;

	/**
	 * Pour se connecter a l'interface
	 */
	private String connect;

	public static int id_agence;
	

	/**
	 * Constructeur 1 
	 * @param identifiant
	 * @param mot_de_passe
	 * @param connect
	 */
	public Utilisateur(String identifiant, String mot_de_passe,String connect) {
		this.setIdentifiant(identifiant);
		this.setMot_de_passe(mot_de_passe);
		this.connect = connect;
	}

	/**
	 * Constructeur 2
	 */
	public Utilisateur() {
		this.setIdentifiant(identifiant);
		this.setMot_de_passe(mot_de_passe);
	}

	/**
	 * Constructeur 3
	 * @param texte
	 * @param application2
	 */	
	public Utilisateur(String texte, Application application2) {
		super(texte);
		this.application = application2;
	}

	/**
	 * Getters
	 */
	public String getIdentifiant() {
		return identifiant;
	}

	public String getConnect() {
		return connect;
	}
	public String getMot_de_passe() {
		return mot_de_passe;
	}

	/**
	 * Setters
	 */
	public String setConnect(String connect) {
		return this.connect = connect;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	public void setMot_de_passe(String mot_de_passe) {
		this.mot_de_passe = mot_de_passe;
	}

	/**
	 * Methode "seconnecter" c'est-a-dire pour se connecter
	 */
	public void actionPerformed(ActionEvent e) {
		String identifiant = JOptionPane.showInputDialog(null, "Identifiant","Authentification",JOptionPane.QUESTION_MESSAGE);
		String mot_de_passe = JOptionPane.showInputDialog(null, "Mot de Passe","Authentification",JOptionPane.QUESTION_MESSAGE);

		Connection conn = SingletonConnection.getInstance();
		PreparedStatement state;
		int nb = 0;
		try {
			state = conn.prepareStatement("SELECT id_agence FROM compte WHERE identifiant = '" + identifiant + "' AND mot_de_passe = '" + mot_de_passe + "'");
			ResultSet result = state.executeQuery();

			if (result.next() == false) {
				JOptionPane.showMessageDialog(null,  "Erreur d'authentification, Veuillez reessayer.", "Authentification", JOptionPane.INFORMATION_MESSAGE);
				connect = "pas connecte";
			}
			else {
				JOptionPane.showMessageDialog(null,  "Authentification reussie !", "Authentification", JOptionPane.INFORMATION_MESSAGE);
				id_agence = result.getInt("id_agence");
				connect = "connecte";
				System.out.println(connect);
				
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		if(identifiant.equals("siege") && mot_de_passe.equals("mdp1234")) {
			connect = "connecteAsSiege";
		}

		if (connect.equals("connecte")) {
			Application lochappy = new Application(connect);
			application.dispose();
		}
		if(connect.equals("connecteAsSiege") ) {
			Application lochappy = new Application("connecteAsSiege");
			application.dispose();
			
		}
	}
}


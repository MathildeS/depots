package squelette;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import action.Application;
import bdd.AgenceManager;
import parc_auto.Moteur;
import parc_auto.Puissance;
import parc_auto.Type;
import parc_auto.Voiture;

/**
 * Classe agence qui s'occupe de :
 * 		- initialiser des agences
 * 		- calculer des rendements sur l'agence
 * 		- d'avoir acces au parc automobile de chaque agence 
 * 		- etc ...
 * @author groupe LocHappy
 *
 */
public class Agence extends AbstractAction  {

	/**
	 * Attributs de l'agence 
	 */
	private int id_agence; 

	private String adresse;

	private String ville;

	private String code_postal;

	private String pays;

	/**
	 * Liste des voitures dans l'agence
	 */
	private List<Voiture> voitures;

	/**
	 * Liste des clients qui ont deja reserve dans l'agence
	 */
	private List<Client> clients;

	/**
	 * Liste des reservations de l'agence
	 */
	private List<Reservation> reservations;

	/**
	 * Liste des voitures qui entrent dans l'agence suite a un retour de vehicule 
	 */
	private List<Voiture> enregistrerEntree;

	/**
	 * Liste des voitures qui sortent dans l'agence suite a un retour
	 *  de vehicule dans une autre agence
	 */
	private List<Voiture> enregistrerSortie;

	private Application application;

	/**
	 * Constructeur 1
	 * @param adresse
	 * @param Ville
	 * @param code_postal
	 * @param pays
	 */
	public Agence(int id_agence, String adresse, String Ville, String code_postal, String pays) {
		this.id_agence = id_agence;
		this.adresse =adresse;
		this.ville = Ville;
		this.code_postal = code_postal;
		this.pays= pays;
	}

	/**
	 * Constructeur 2
	 * @param voitures
	 * @param clients
	 * @param reservations
	 */
	public Agence(List<Voiture> voitures, List<Client> clients, List<Reservation> reservations) {
		this.voitures =  voitures;
		this.clients = clients;
		this.reservations = reservations;
	}

	/**
	 * Constructeur 3
	 * @param enregistrer_sortie
	 * @param enregistrer_entree
	 */
	public Agence(List<Voiture> enregistrer_sortie, List<Voiture> enregistrer_entree) {
		this.enregistrerEntree = enregistrer_entree;
		this.enregistrerSortie = enregistrer_sortie;
	}

	/**
	 * Constructeur 4
	 * @param id_agence
	 */
	public Agence(int id_agence) {
		this.id_agence = id_agence;
	}

	/**
	 * Constructeur 5
	 * @param appli
	 * @param texte
	 */
	public Agence(String texte,Application appli) {
		super(texte);
		this.setApplication(appli);
	}

	/**
	 * Getteurs
	 */

	public String getAdresse() {
		return this.adresse;
	}

	public String getVille() {
		return this.ville;
	}

	public String getCode_postal() {
		return this.code_postal;
	}

	public String getPays() {
		return this.pays;
	}
	public List<Voiture> getVoitures() {
		return voitures;
	}

	public List<Client> getClients() {
		return clients;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	/**
	 * Setter
	 */
	public void setVoitures(List<Voiture> voitures) {
		this.voitures = voitures;
	}

	/**
	 * Methode d'enregistrement des voitures qui entrent de l'agence
	 * @param voitureE
	 * @param reservation
	 * @throws SQLException
	 */
	public List<Voiture> enregistrerEntree(Voiture voitureE, Reservation reservation) throws SQLException{

		if(reservation.getAgence_retour() == this.id_agence) { 
			List<Voiture> enregistrerEntree = new ArrayList<Voiture>();
			enregistrerEntree.add(voitureE); 
		}
		else { 
			JOptionPane d = new JOptionPane();
			int retour = JOptionPane.showConfirmDialog(null, "Voiture en entretien?","Entretien",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

			if( retour == JOptionPane.OK_OPTION) { // L'entier correspondant a l'indice du bouton clique vaut 0 (donc Oui)
				JOptionPane.showMessageDialog(null,"La voiture est en entretien", "Votre reponse",JOptionPane.INFORMATION_MESSAGE);
			}
			else { // L'entier correspondant a l'indice du bouton clique vaut 1 (donc Non)
				JOptionPane.showMessageDialog(null, "La voiture n'est pas en entretien","Votre reponse",JOptionPane.INFORMATION_MESSAGE);
			}
		}
		return enregistrerEntree;
	}

	/**
	 * Methode d'enregistrement des voitures qui sortent de l'agence
	 * @param voitureS
	 * @param reservation
	 * @throws SQLException
	 */
	public List<Voiture> enregistrerSortie(Voiture voitureS, Reservation reservation) throws SQLException {

		if(reservation.getAgence_depart() == this.id_agence) { 
			enregistrerSortie = new ArrayList<Voiture>();
			enregistrerSortie.add(voitureS); 
		}
		else { 
			int retour = JOptionPane.showConfirmDialog(null, "Voiture en entretien?","Entretien",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

			if( retour == JOptionPane.OK_OPTION) { // L'entier correspondant a l'indice du bouton clique vaut 0 (donc Oui)
				JOptionPane.showMessageDialog(null,"La voiture est en entretien", "Votre reponse",JOptionPane.INFORMATION_MESSAGE);
			}

			else { // L'entier correspondant a l'indice du bouton clique vaut 1 (donc Non)
				JOptionPane.showMessageDialog(null, "La voiture n'est pas en entretien","Votre reponse",JOptionPane.INFORMATION_MESSAGE);
			}
		}
		return enregistrerSortie;
	}

	/**
	 * Calcul du rendement 
	 * en calculant le nombre de voitures qui sortent
	 * par rapport a celles qui entrent
	 * @return le rendement de l'agence 
	 * @throws SQLException 
	 */
	public float calculrendement(int id_agence) throws SQLException {

		float investissement = 5000; // Entreprise depense 10000 euros pour chaque agence
		Connection conn = SingletonConnection.getInstance();
		String sql = "SELECT count(*) AS nb_voitures FROM reservation AS r WHERE r.agence_depart = ?"; 
		PreparedStatement state = conn.prepareStatement(sql);
		state.setInt(1,id_agence);
		ResultSet resultSet = state.executeQuery();
		int nb_voiture = 0;
		if(resultSet.next()) {
			int nb_voitures = resultSet.getInt("nb_voitures"); // Le nombre de voitures reservees dans cette agence
		}

		String sqlVoitures = "SELECT r.id_voiture, r.nb_jours FROM reservation AS r WHERE r.agence_depart = ?";
		PreparedStatement stateVoitures = conn.prepareStatement(sqlVoitures);
		stateVoitures.setInt(1, id_agence);
		ResultSet resultSetVoitures = stateVoitures.executeQuery();

		List<Integer> list_idV = new ArrayList<Integer>();
		List<Integer> list_nbjour = new ArrayList<Integer>();
		float gain = 0;
		while (resultSetVoitures.next()) {
			int id = resultSetVoitures.getInt("id_voiture");
			int nb_jour = resultSetVoitures.getInt("nb_jours");
			list_idV.add(id);
			list_nbjour.add(nb_jour);
		}

		// On cherche a calculer le gain de l'agence
		for(int i=0; i<list_idV.size(); i++) {
			String sql1 = "SELECT v.prix, v.id_type, v.id_moteur, v.id_puissance FROM voiture AS v WHERE v.id_voiture = ?";
			PreparedStatement state1 = conn.prepareStatement(sql1);
			state1.setInt(1, list_idV.get(i) );
			ResultSet resultat = state1.executeQuery();

			Reservation reservation = new Reservation(list_idV.get(i));

			int prix = 0;
			int id_type = 0;
			int id_puissance = 0;
			int id_moteur = 0;
			while (resultat.next()) {
				prix = resultat.getInt("prix");
				id_type = resultat.getInt("id_type");
				id_moteur = resultat.getInt("id_moteur");
				id_puissance = resultat.getInt("id_puissance");
			}
			Type type = new Type(id_type);
			Moteur moteur = new Moteur(id_moteur);
			Puissance puissance = new Puissance(id_puissance);
			float prix_final_v = reservation.prixFinal(prix,list_idV.get(i), puissance,type, moteur,list_nbjour.get(i));
			gain = gain + prix_final_v;
		}	
		float rendement = gain - investissement;
		return rendement;
	}

	/**
	 * Methode herite interaction avec le siege
	 * via le bouton bilan rendement
	 */
	public void actionPerformed(ActionEvent e) {
		Connection conn = SingletonConnection.getInstance();
		String sql = "SELECT id_agence FROM agence AS a "; 
		PreparedStatement state;
		List<Integer> list_Idagence = new ArrayList<Integer>();
		try {
			state = conn.prepareStatement(sql);
			ResultSet resultSet = state.executeQuery();
			while(resultSet.next()) {
				list_Idagence.add(resultSet.getInt("id_agence"));
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		float total = 0;

		String message = "Bilan rendement : \n\n";
		for(int i=0; i<list_Idagence.size();i++) {
			int id_agence = list_Idagence.get(i);
			Agence agence = new Agence(id_agence);
			try {
				message+="Agence "+id_agence+" = "+agence.calculrendement(id_agence)+" euros\n\n";
				total+=agence.calculrendement(id_agence);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		message +="Rendement total de l'entreprise  = "+total+" euros";
		JOptionPane.showMessageDialog(null, message ,"Bilan rendement",JOptionPane.INFORMATION_MESSAGE);
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
}






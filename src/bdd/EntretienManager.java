package bdd;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import action.Application;
import parc_auto.Entretien;
import squelette.SingletonConnection;

/**
 * Classe permettant d'interargir avec la BDD et l'utilisateur
 * @author groupe Lochappy
 *
 */
public class EntretienManager  extends AbstractAction{

	private Application application;

	/**
	 * Constructeur 1
	 * @param texte
	 * @param appli
	 */
	public EntretienManager(String texte, Application appli) {
		super(texte);
		this.application = appli;
	}

	/**
	 * Constructeur 2 
	 */
	public EntretienManager() {
	}

	/**
	 * Methode permettant de sauvegarder et de creer les entretiens dans
	 * la BDD
	 * @param entretien
	 * @param id_voiture
	 * @return une valeur exub�rante si l'entretien n'existe pas
	 * @throws SQLException
	 */
	public static int save(Entretien entretien, int id_voiture) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql = "INSERT INTO entretien(id_voiture,date_entree, date_sortie) VALUES (?,?,?)";
		PreparedStatement state = conn.prepareStatement(sql);
		state.setInt(1, id_voiture);
		state.setString(2, entretien.getDate_entree());
		state.setString(3, entretien.getDate_sortie());
		state.execute();
		state.close();

		String sqlId = "SELECT * FROM entretien_id_seq";
		Statement statement = conn.createStatement();
		ResultSet resultSet = statement.executeQuery(sqlId);
		if (resultSet.next()) {
			return resultSet.getInt("last_value");
		}
		return -1;
	}

	/**
	 * Methode permettant de recuperer des donnees sur un entretien
	 * @param id_entretien
	 * @return liste des donnees sur un entretien grace a son identifiant
	 * @throws SQLException
	 */
	public List<String> recupDonnesEntretien(int id_entretien) throws SQLException{
		Connection conn = SingletonConnection.getInstance();
		String sql = " SELECT * \r\n" + 
				" FROM entretien AS e  \r\n" + 
				" WHERE e.id_entretien = ?" ; 
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1, id_entretien);
		ResultSet resultat = statement.executeQuery();
		List<String> donne = new ArrayList<String>() ;
		while (resultat.next()){
			String id_voiture = resultat.getString("id_voiture");
			String date_entree = resultat.getString("date_entree");
			String date_sortie = resultat.getString("date_sortie");

			donne.add(id_voiture);
			donne.add(date_entree);
			donne.add(date_sortie);
		}
		return donne;
	}

	@Override
	/**
	 * Methode herite permettant l'interaction avec l'utilisateur
	 * via le bouton gerer les entretiens
	 */
	public void actionPerformed(ActionEvent arg0) {
		
		Connection conn = SingletonConnection.getInstance();

		String[] poss = {"Consulter un entretien","Retirer un vehicule de l'entretien","Placer un vehicule en entretien"};
		String reponse = (String)JOptionPane.showInputDialog(null, "Que voulez-vous faire ?","Gerer les entretiens",JOptionPane.QUESTION_MESSAGE,null,poss,poss[0]);

		if(reponse.contentEquals("Consulter un entretien")) {
			String num_entretien = JOptionPane.showInputDialog(null, "Numero de l'entretien","Consulter un entretien",JOptionPane.QUESTION_MESSAGE);
			int id_entretien = Integer.parseInt(num_entretien);

			EntretienManager entretienmana = new EntretienManager();
			List<String> donnee = null;
			try {
				donnee = entretienmana.recupDonnesEntretien(id_entretien);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			int id_voiture = Integer.parseInt(donnee.get(0));
			String date_entree = donnee.get(1).replaceAll("\\s", "");
			String date_sortie = donnee.get(2).replaceAll("\\s", "");

			VoitureManager voituremana = new VoitureManager();
			List<String> donnee_v = null;
			try {
				donnee_v = voituremana.recupDonneesVoiture(id_voiture);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}

			String marque_voiture = donnee_v.get(1).replaceAll("\\s", "");
			String modele_voiture = donnee_v.get(2).replaceAll("\\s", "");
			String immatriculation = donnee_v.get(0).replaceAll("\\s", "");

			String message = "Numero entretien : "+ id_entretien +"\n\n"+
					"Entretien d'une "+ marque_voiture + "  "+ modele_voiture+"  immatricule  "+
					immatriculation +"\n\nDate d'entree : " + date_entree+"     "+"Date de sortie : "+
					date_sortie;
			JOptionPane.showMessageDialog(null, message,"Consulter un entretien",JOptionPane.INFORMATION_MESSAGE);
		}

		else if(reponse.contentEquals("Retirer un vehicule de l'entretien")) {
			String immatriculation = JOptionPane.showInputDialog(null, "Immatriculation du vehicule","Retirer un vehicule de l'entretien",JOptionPane.QUESTION_MESSAGE);
			if(immatriculation == null) {
				this.application.dispose();			
				}
			else {
			VoitureManager voituremanag = new VoitureManager();
			int id_voiture= 0;
			try {
				id_voiture = voituremanag.bringBackId(immatriculation);
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
			// SI LA VOITURE EXISTE
			if (id_voiture!=0) {
				// On supprime l'entretien
				String sql = " DELETE FROM entretien AS e  \r\n" + 
						" WHERE e.id_voiture = ?" ; 
				try {
					PreparedStatement statement = conn.prepareStatement(sql);
					statement.setInt(1, id_voiture);
					ResultSet resultat = statement.executeQuery();
					resultat.next();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				// On rend disponnible la voiture a la location
				String sql_v = "UPDATE voiture SET entretien='oui' WHERE id_voiture=?";
				PreparedStatement statement;
				try {
					statement = conn.prepareStatement(sql_v);
					statement.setInt(1, id_voiture);
					statement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "Ce vehicule a ete retire de l'entretien","Retirer un vehicule de l'entretien",JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				JOptionPane.showMessageDialog(null, "Ce vehicule n'existe pas dans les entretiens","Retirer un vehicule de l'entretien",JOptionPane.INFORMATION_MESSAGE);
			}
			}
		}
		// PLACER UN VEHICULE EN ENTRETIEN
		else {
			String[] jour = {"01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
			String[] mois = {"01","02","03","04","05","06","07","08","09","10","11","12"};

			String num_voiture = JOptionPane.showInputDialog(null, "Identifiant du vehicule","Placer un vehicule en entretien",JOptionPane.QUESTION_MESSAGE);
			String jour_entree = (String) JOptionPane.showInputDialog(null, "Jour d'entree du vehicule en entretien","Placer un vehicule en entretien",JOptionPane.QUESTION_MESSAGE,null,jour,jour[0]);
			String mois_entree = (String) JOptionPane.showInputDialog(null, "Mois d'entree du vehicule en entretien","Placer un vehicule en entretien",JOptionPane.QUESTION_MESSAGE,null,mois,mois[0]);

			String jour_sortie = (String) JOptionPane.showInputDialog(null, "Jour de sortie du vehicule en entretien","Placer un vehicule en entretien",JOptionPane.QUESTION_MESSAGE,null,jour,jour[0]);
			String mois_sortie = (String) JOptionPane.showInputDialog(null, "Mois de sortie du vehicule en entretien","Placer un vehicule en entretien",JOptionPane.QUESTION_MESSAGE,null,mois,mois[0]);

			int id_voiture = Integer.parseInt(num_voiture);
			String date_entree = jour_entree+"/"+mois_entree;
			String date_sortie = jour_sortie+"/"+mois_sortie;

			Entretien entretien = new Entretien(date_entree, date_sortie);
			EntretienManager entretien_manag = new EntretienManager();
			try {
				entretien_manag.save(entretien, id_voiture);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}

			// On rend indisponible la voiture a la location
			String sql_v = "UPDATE voiture SET entretien='non' WHERE id_voiture=?";
			PreparedStatement statement;
			try {
				statement = conn.prepareStatement(sql_v);
				statement.setInt(1, id_voiture);
				statement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, "Le vehicule a bien ete ajouter a entretien","Placer un vehicule en entretien",JOptionPane.INFORMATION_MESSAGE);
		}
	}
}



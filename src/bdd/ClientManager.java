package bdd;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import action.Application;
import squelette.SingletonConnection;

/**
 * Classe ClientManager permettant d'interagir avec la BDD et l'utilisateur
 * @author groupe Lochappy
 *
 */
public class ClientManager extends AbstractAction {

	private Application application;

	/**
	 * Constructeur1
	 */
	public ClientManager() {
	}

	/**
	 * Constructeur 2
	 * @param texte
	 * @param appli
	 */
	public ClientManager(String texte, Application appli) {
		super(texte);
		this.application = appli;
	}

	/**
	 * Methode permettant de recuperer les donnes du client via sa fiche client
	 * @param nom
	 * @param prenom
	 * @param permis
	 * @return La liste des donnees sur un client � l'aide de son nom, prenom et permis
	 * @throws SQLException
	 */
	public List<String> recupDonneesClient(String nom, String prenom, String permis) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql = " SELECT * \n" + 
				" FROM fiche_client AS c \r\n" + 
				" WHERE c.nom= ? \n" + 
				"AND c.prenom = ? \n" + 
				"AND c.permis = ? ";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1,nom);
		statement.setString(2,prenom);
		statement.setString(3,permis);
		ResultSet resultat = statement.executeQuery();
		List<String> donne = new ArrayList<String>() ;
		while (resultat.next()){
			String id_client = resultat.getString("id_client");
			String date_naissance = resultat.getString("date_de_naissance");
			String num_telephone = resultat.getString("num_telephone");
			String adresse_mail = resultat.getString("adresse_mail");
			String code_postal = resultat.getString("code_postal");
			String adresse = resultat.getString("adresse");
			String ville = resultat.getString("ville");
			String pays = resultat.getString("pays");

			donne.add(date_naissance);
			donne.add(num_telephone);
			donne.add(adresse_mail);
			donne.add(adresse);
			donne.add(ville);
			donne.add(code_postal);
			donne.add(pays);
			donne.add(id_client);
		}
		return donne;
	}

	/**
	 * Methode permettant de recuperer les donnes du client via fiche client
	 * @param nom
	 * @param prenom
	 * @return liste des donnees sur un client � l'aide de son identifiant
	 * @throws SQLException
	 */
	public List<String> recupDonneClient(String nom, String prenom) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql = " SELECT * \n" + 
				" FROM fiche_client AS c \r\n" + 
				" WHERE nom = ? AND prenom = ?";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1,nom);
		statement.setString(2,prenom);
		ResultSet resultat = statement.executeQuery();
		List<String> donne = new ArrayList<String>() ;
		while (resultat.next()){
			String id_client = resultat.getString("id_client");
			String permis = resultat.getString("permis");
			String date_naissance = resultat.getString("date_de_naissance");
			String num_telephone = resultat.getString("num_telephone");
			String adresse_mail = resultat.getString("adresse_mail");
			String code_postal = resultat.getString("code_postal");
			String adresse = resultat.getString("adresse");
			String ville = resultat.getString("ville");
			String pays = resultat.getString("pays");

			donne.add(date_naissance);
			donne.add(num_telephone);
			donne.add(adresse_mail);
			donne.add(adresse);
			donne.add(ville);
			donne.add(code_postal);
			donne.add(pays);
			donne.add(id_client);
			donne.add(permis);
		}
		return donne;
	}

	/**
	 *  Methode permettant de recuperer les donnes du client via fiche client
	 * @param id_client
	 * @return liste des donnees sur un client � l'aide de son identifiant
	 * @throws SQLException
	 */
	public List<String> recupDonnesClient(int id_client) throws SQLException {
		Connection conn = SingletonConnection.getInstance();
		String sql = " SELECT * \n" + 
				" FROM fiche_client AS c \r\n" + 
				" WHERE id_client = ?";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1,id_client);;
		ResultSet resultat = statement.executeQuery();
		List<String> donne = new ArrayList<String>() ;
		while (resultat.next()){
			String nom = resultat.getString("nom");
			String prenom = resultat.getString("prenom");
			String permis = resultat.getString("permis");
			String date_naissance = resultat.getString("date_de_naissance");
			String num_telephone = resultat.getString("num_telephone");
			String adresse_mail = resultat.getString("adresse_mail");
			String code_postal = resultat.getString("code_postal");
			String adresse = resultat.getString("adresse");
			String ville = resultat.getString("ville");
			String pays = resultat.getString("pays");

			donne.add(date_naissance);
			donne.add(num_telephone);
			donne.add(adresse_mail);
			donne.add(adresse);
			donne.add(ville);
			donne.add(code_postal);
			donne.add(pays);
			donne.add(nom);
			donne.add(prenom);
			donne.add(permis);
		}
		return donne;
	}

	@Override
	/**
	 * Methode herite permettant l'interaction avec le siege
	 * via le bouton consulter client
	 */
	public void actionPerformed(ActionEvent e) {

		String nom = JOptionPane.showInputDialog(null,"Nom du client","Consulter client",JOptionPane.QUESTION_MESSAGE);
		String prenom = JOptionPane.showInputDialog(null,"Prenom du client","Consulter client",JOptionPane.QUESTION_MESSAGE);

		// RECUPERATION ID_CLIENT
		ClientManager client_manag = new ClientManager();
		List<String> donnees = null;
		try {
			donnees = client_manag.recupDonneClient(nom, prenom);

		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		String idclient = donnees.get(0);
		int id_client = Integer.parseInt(idclient);

		//SI LE CLIENT EXISTE
		if(id_client!=0) {
			String adresse_mail = donnees.get(4).replaceAll("\\s", "");
			String num_telephone = donnees.get(3).replaceAll("\\s", "");
			String adresse = donnees.get(4);
			String code_postal = donnees.get(5).replaceAll("\\s", "");
			String ville = donnees.get(7).replaceAll("\\s", "");
			String pays = donnees.get(8).replaceAll("\\s", "");
			// CREATION BILAN SUR LE CLIENT
			String message = "Fiche client numero "+id_client+"\n\n"+
					nom + "   "+ prenom +"   N telephone  "+ num_telephone +"   "+adresse_mail+"\n\n"+
					"Adresse : " + adresse +"  "+ code_postal +"  "+ville+"  "+pays;
			JOptionPane.showMessageDialog(null, message,"Consultation de la fiche client",JOptionPane.INFORMATION_MESSAGE);

			// RENVOYER SI LE CLIENT A UNE RESERVATION EN COURS
			String sql1 = " SELECT id_reservation \n" + 
					" FROM reservation AS r \r\n" + 
					" WHERE r.id_client= ? \n";
			int id_reservation = 0;
			try {
				Connection conn = SingletonConnection.getInstance();
				PreparedStatement statement1 = conn.prepareStatement(sql1);
				statement1.setInt(1,id_client);
				ResultSet resultat_reserv = statement1.executeQuery();
				while (resultat_reserv.next()){
					id_reservation = resultat_reserv.getInt("id_reservation");
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			if(id_reservation == 0) {
				JOptionPane.showMessageDialog(null, "Ce client n'a pas reserve de vehicule recemment dans notre agence","Consultation de la fiche client",JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				JOptionPane.showMessageDialog(null, "Ce client a reserve un vehicule.\n numero de reservation : "+id_reservation,"Consultation de la fiche client",JOptionPane.INFORMATION_MESSAGE);
			}
		}
		else {
			JOptionPane.showMessageDialog(null,"Ce client n'existe pas. " ,"Consulter fiche_client",JOptionPane.INFORMATION_MESSAGE);
		}
	}
}
